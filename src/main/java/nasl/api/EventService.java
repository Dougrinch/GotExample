package nasl.api;

import java.util.List;

public interface EventService {

    boolean insert(Event event);

    void removeAll();

    List<Event> getAllFromLastHour();
}
