package nasl.impl;

import nasl.api.Event;
import nasl.api.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class EventServiceImpl implements EventService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<Event> rowMapper;

    public EventServiceImpl() {
        rowMapper = (rs, rowNum) -> {
            Event event = new Event();
            event.id = rs.getLong("id");
            event.eventTime = rs.getDate("eventTime");
            return event;
        };
    }

    @Override
    public boolean insert(Event event) {
        return jdbcTemplate.update("INSERT INTO Event (eventTime) VALUES (?)", event.eventTime) > 0;
    }

    @Override
    public void removeAll() {
        jdbcTemplate.update("DELETE FROM Event");
    }

    @Override
    public List<Event> getAllFromLastHour() {
        return jdbcTemplate.query("SELECT id, eventTime FROM Event WHERE eventTime < ?", rowMapper, Date.from(Instant.now().minus(Duration.ofHours(1))));
    }
}
