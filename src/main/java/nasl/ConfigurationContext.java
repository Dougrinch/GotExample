package nasl;

import liquibase.integration.spring.SpringLiquibase;
import nasl.api.EventService;
import nasl.impl.EventServiceImpl;
import org.h2.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:main.properties")
public class ConfigurationContext {

    @Bean
    public SimpleDriverDataSource getDataSource(Environment env) {
        return new SimpleDriverDataSource(new Driver(), env.getProperty("jdbc.url"));
    }

    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public SpringLiquibase getSpringLiquibase(DataSource dataSource) {
        SpringLiquibase springLiquibase = new SpringLiquibase();
        springLiquibase.setDataSource(dataSource);
        springLiquibase.setChangeLog("classpath:changelog.xml");
        return springLiquibase;
    }

    @Bean
    public EventService getService() {
        return new EventServiceImpl();
    }
}
