package nasl;

import nasl.api.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.Date;

import static java.lang.Math.abs;
import static org.dou.got.TimeSetter.setTime;
import static org.testng.Assert.assertTrue;

@ContextConfiguration(classes = ConfigurationContext.class)
public class MainTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EventService eventService;

    @DataProvider
    private Object[][] setTimeVariants() {
        return new Object[][]{{0}, {500000}, {-1}};
    }

    @Test(dataProvider = "setTimeVariants")
    public void setTimeAlsoWorksInInMemoryDatabase(long time) {
        setTime(time);
        if (time == -1) // setTime(-1) == revertTime()
            time = System.currentTimeMillis();

        long dbTime = jdbcTemplate.queryForObject("SELECT sysdate() FROM dual", Date.class).getTime();

        assertTimeEquals(time, dbTime, 100);
    }

    private void assertTimeEquals(long first, long second, long delta) {
        long timeDelta = second - first;

        assertTrue(abs(timeDelta) < delta, "timeDelta == " + timeDelta + ", first = " + Instant.ofEpochMilli(first) + ", second = " + Instant.ofEpochMilli(second));
    }
}
